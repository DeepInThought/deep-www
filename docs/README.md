Welcome to https://docs.deepinthought.io
===========================
By [@DeepInThought](https://github.com/DeepInThought). 

We're in the early stages of some amazing stuff!  Please stay tuned for more.  

About DeepInThought
-------
[@DeepInThought](https://github.com/DeepInThought) is a [St. Louis, Missouri](https://en.wikipedia.org/wiki/St._Louis) tech startup.  
* Official website is in development.
* Feel free to contact us at [support@deepinthought.io](mailto:support@deepinthought.io).  
* [LinkedIn](https://www.linkedin.com/company/deepinthought_io/)
* [Twitter](https://twitter.com/deepITio) 

![DeepIT](https://raw.githubusercontent.com/DeepInThought/deep-www/master/docs/images/deep_main.png)
